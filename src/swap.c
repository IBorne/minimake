#include "swap.h"

bool is_var(char *key)
{
    if (!key)
        return false;
    size_t len = strlen(key);
    if (len < 4)
        return false;
    if (key[0] == '$')
    {
        if (key[1] == '{')
            return key[len - 1] == '}';
        if (key[1] == '(')
            return key[len - 1] == ')';
        return true;
    }
    else
        return false;
}

static bool sound(char *data, struct list *previous)
{
    if (!data)
        return true;
    while (previous)
    {
        if (strcmp(previous->line, data) == 0)
            return false;
        previous = previous->next;
    }
    struct list *next = malloc(sizeof(struct list));
    next->line = malloc(sizeof(struct list));
    next->line = strcpy(next->line, data);
    previous->next = next;
    return true;
}

static void free_previous(struct list *previous)
{
    if (previous)
    {
        if (previous->next)
            free_previous(previous->next);
        free(previous->line);
        free(previous);
    }
}

char *swap(char *key)
{
    struct data *var = search(key);
    if (!var)
    {
        key = memset(key, '\0', strlen(key));
        key[0] = ' ';
        return key;
    }
    char *data = var->data;
    struct list *previous = malloc(sizeof(struct list));
    previous->line = strdup(data);
    previous->next = NULL;
    while (is_var(data))
    {
        var = search(key);
        data = var->data;
        if (!sound(data, previous))
            errx(2, "*** Recursive variable '%s' references itself (eventually). Stop.", data);
    }
    free_previous(previous);
    /*key = memset(key, '\0', strlen(key));
    *key = realloc(key, sizeof(data) * strlen(data));
    memcpy(key, data, strlen(data));*/
    free(key);
    char *return_value = strdup(data);
    return return_value;
}

static void check_error(char *line)
{
    size_t len = strlen(line);
    int braces = 0;
    int bracket = 0;
    for (size_t i = 0; i < len; i++)
    {
        if (i < (len -1) && line[i] == '$' && line[i + 1] == '(')
        {
            braces += 1;
        }
        else if (line[i] == ')')
        {
            braces -= 1;
            if (braces < 0)
                errx(2, "*** unterminated variable reference. Stop.");
        }
        else if (i < (len -1) && line[i] == '$' && line[i + 1] == '{')
        {
            bracket += 1;
        }
        else if (line[i] == '}')
        {
            bracket -= 1;
            if (bracket < 0)
                errx(2, "*** unterminated variable reference. Stop.");
        }
    }
    if (braces != 0 || bracket != 0)
        errx(2, "*** unterminated variable reference. Stop.");
}

static int to_array(char **arr, char *line)
{
    char *token = strtok(line, "$");
    int i = 0;
    while (token)
    {
        /*arr[i] = malloc(sizeof(token) * strlen(token));
        arr[i] = strcpy(arr[i], token);*/
        arr[i] = strdup(token);
        i++;
        token = strtok(NULL, "$");
    }
    for (int j = 0; j < i; j++)
    {
        if (arr[j][0] == '{')
        {
            arr[j] = strtok(arr[j], "}");
            for (int k = i + 1; k > (j + 1); k--)
                arr[k] = arr[k - 1];
            char *rest = strtok(NULL, "\\");
            if (rest && strlen(rest) > 0)
            {
                arr[j + 1] = strdup(rest);
                i++;
            }
        }
        else if (arr[j][0] == '(')
        {
            arr[j] = strtok(arr[j], ")");
            for (int k = i + 1; k > (j + 1); k--)
                arr[k] = arr[k - 1];
            char *rest = strtok(NULL, "\\");
            if (rest && strlen(rest) > 0)
            {
                arr[j + 1] = strdup(rest);
                i++;
            }
        }
    }
    return i;
}

char *expand(char *line)
{
    check_error(line);
    char **arr = malloc(sizeof(char *) * SIZE);
    int i = to_array(arr, line);
    int k = (line[0] == '$' ? 0 : 1);
    for (; k < i; k++)
    {
        if (arr[k][0] == '{' || arr[k][0] == '(')
        {
            memmove(arr[k], arr[k] + 1, strlen(arr[k]));
            arr[k] = swap(arr[k]);
        }
    }
    size_t len  = 0;
    for (int j = 0; j < i; j++)
    {
        if (arr[j])
            len += strlen(arr[j]);
    }
    line = memset(line, '\0', strlen(line));
    line = realloc(line, len + 1);
    for (int j = 0; j < i; j++)
    {
        if (arr[j] && strlen(arr[j]) > 1)
        {
           line = strcat(line, arr[j]);
           free(arr[j]);
        }
        else
            line = strcat(line, " ");
    }
    free(arr);
    return line;
}
