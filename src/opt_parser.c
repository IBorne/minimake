#include "minimake.h"

void print_usage(void)
{
    printf("Usage: ./Makefile [-h|-f] [file]\n");
}

static struct option_sh *init_option(void)
{
    struct option_sh *opt = malloc(sizeof(struct option_sh));
    if (!opt)
        return NULL;
    opt->all = false;
    opt->f = false;
    opt->file = NULL;
    opt->h = NULL;
    opt->makefile = NULL;
    struct data *table = malloc(sizeof(struct data));
    if (!table)
        errx(1, "memory exhausted");
    table->key = "head";
    table->data = "head";
    table->next = NULL;
    opt->table = table;
    struct rule *rules = malloc(sizeof(struct rule));
    if (!rules)
        errx(1, "memory exhausted");
    rules->target = "head";
    rules->prere = "head";
    rules->recipe = NULL;
    rules->next = NULL;
    opt->rule = rules;
    return opt;
}

struct option_sh *opt_parse(int argc, char *argv[])
{
    struct option_sh *opt = init_option();
    if (!opt)
        errx(1, "memory exhausted");
    int opt_ix = 1;
    for (; opt_ix < argc; opt_ix++)
    {
        if (strcmp(argv[opt_ix], "all") == 0)
            opt->all = true;
        if (strcmp(argv[opt_ix], "-h") == 0)
            opt->h = true;
        if (strcmp(argv[opt_ix], "-f") == 0)
        {
            opt->f = true;
            if (opt_ix == argc - 1)
                errx(2, "option require an argument -- 'f'");
            opt->file = argv[opt_ix + 1];
        }
    }
    return opt;
}
