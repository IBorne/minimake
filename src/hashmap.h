#pragma once
#include "minimake.h"

struct data
{
    char* key;
    char* data;
    struct data *next;
};

struct data *search(char *key);
void insert(char *key, char *data);
void delete(char *key);
void clear(void);
void display(void);