#include "minimake.h"

//recipe struct method
void add_recipe(struct list *l, char *line)
{
    while (l->next)
        l = l->next;
    struct list *new = malloc(sizeof(struct list));
    if (!new)
        errx(1, "exhausted memory");
    if (line)
    {
        new->line = malloc(sizeof(line) * strlen(line));
        new->line = strcpy(new->line, line);
    }
    else
    {
        new->line = NULL;
    }
    new->next = NULL;
    l->next = new;
}

//rule struct method
struct rule *retrieve(char *target)
{
    struct rule *table = g_opt->rule;
    while (table)
    {
        if (strcmp(table->target, target) == 0)
            return table;
        table = table->next;
    }
    return NULL;
}

void add(char *target, char *prere, struct list *recipe)
{
    struct rule *item = malloc(sizeof(struct rule));
    if (!item)
        errx(1, "exhausted memory");
    item->target = target;
    item->prere = prere;
    item->recipe = recipe;
    item->next = NULL;

    struct rule *table = g_opt->rule;

    while (table->next)
        table = table->next;
    table->next = item;
}

void suppr(char *target)
{
    struct rule *table = g_opt->rule;
    while (table->next && strcmp(table->next->target, target) != 0)
        table = table->next;

    struct rule *temp = table->next;
    table->next = table->next->next;

    free(temp->target);
    free(temp->prere);
    free(temp->recipe);
    free(temp);
}

static void free_list(struct list *l)
{
    if (l->next)
        free_list(l->next);
    free(l);
}

static void relieveit(struct rule *table)
{
    if (table->next)
        relieveit(table->next);
    free(table->target);
    free(table->prere);
    free_list(table->recipe);
    free(table);
}

void relieve(void)
{
    if (g_opt->rule)
        relieveit(g_opt->rule->next);
    free(g_opt->rule);
}

void print_line(struct list *l)
{
    printf("\n\t~%s",l->line);
    if (l->next)
        print_line(l->next);
    else
        printf("]\n");
}

void print(void)
{
    struct rule *table = g_opt->rule;
    while (table)
    {
        printf("[%s ~ %s", table->target, table->prere);
        if (table->recipe)
            print_line(table->recipe);
        else
            printf("]\n");
        table = table->next;
    }
}