#pragma once

#include "minimake.h"

struct list
{
    char *line;
    struct list *next;
};

struct rule
{
    char *target;
    char *prere;
    struct list *recipe;
    struct rule *next;
};

void add_recipe(struct list *l, char *line);
struct rule *retrieve(char *target);
void add(char *target, char *prere, struct list *recipe);
void suppr(char *target);
void relieve();
void print();
