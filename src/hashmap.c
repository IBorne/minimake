#include "minimake.h"

struct data *search(char *key)
{
    struct data *table = g_opt->table;
    while(table)
    {
        if (strcmp(table->key, key) == 0)
            return table;
        table = table->next;
    }
    return NULL;
}

void insert(char *key, char *data)
{
    struct data *exist = search(key);
    if (exist)
    {
        exist->data = realloc(exist->data, sizeof(data) * strlen(data));
        exist->data = strcpy(exist->data, data);
        return;
    }
    struct data *item = malloc(sizeof(struct data));
    if (!item)
        errx(1, "exhausted memory");
    char *item_data = malloc(sizeof(data) * strlen(data));
    item_data = strcpy(item_data, data);
    item->data = item_data;
    char *item_key = malloc(sizeof(key) * strlen(key));
    item_key = strcpy(item_key, key);
    item->key = item_key;
    item->next = NULL;
    struct data *table = g_opt->table;
    while (table->next)
        table = table->next;
    table->next = item;
}

void delete(char *key)
{
    struct data *table = g_opt->table;
    while (table->next && strcmp(table->next->key, key) != 0)
        table = table->next;
    struct data *temp = table->next;
    table->next = table->next->next;
    free(temp->key);
    free(temp->data);
    free(temp);
}

static void clearit(struct data *table)
{
    if (table->next)
        clearit(table->next);
    free(table->key);
    free(table->data);
    free(table);
}

void clear(void)
{
    if (g_opt->table && g_opt->table->next)
        clearit(g_opt->table->next);
    free(g_opt->table);
}

void display(void)
{
    struct data *table = g_opt->table;
    while (table)
    {
        printf("[%s = %s]\n", table->key, table->data);
        table = table->next;
    }
}
