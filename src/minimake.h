#pragma once
#define _GNU_SOURCE
#define _POSIX_C_SOURCE

#define SIZE 20

extern struct option_sh *g_opt;

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <err.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <time.h>

#include "opt_parser.h"
#include "hashmap.h"
#include "getline.h"
#include "rulemap.h"
#include "exec.h"
#include "swap.h"
