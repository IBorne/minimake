#include "getline.h"

static void trim_n(char *str)
{
    size_t len = strlen(str);
    size_t i = 0;
    while (i < len)
    {
        if(str[i] == '\n' || str[i] == '\r')
            str[i] = '\0';
        if (str[i] == '\t')
            str[i] = ' ';
        i+=1;
    }
    if (str[0] == ' ')
    {
        memmove(str, str + 1, strlen(str));
    }
}

static void parse_var()
{
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, g_opt->makefile)) != -1)
    {
        trim_n(line);
        if (strchr(line, '=') != NULL)
        {
            char *key = strtok(line, "=");
            char *data = strtok(NULL, "\\");
            if (!data)
                errx(2, "*** empty variable name. Stop.");
            insert(key,data);
        }
    }
    free(line);
}

static void parse_rule()
{
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    rewind(g_opt->makefile);

    char *target = NULL;
    char *prere = NULL;
    //char c;
    struct list *recipe = NULL;
    while ((read = getline(&line, &len, g_opt->makefile))!= -1)
    {
        trim_n(line);
        if (strchr(line, ':') != NULL)
        {
            target = strtok(line, ":");
            if (!target)
                errx(1, "recipe come before target");
            char *item_target = malloc(sizeof(target) * strlen(target));
            item_target = strcpy(item_target, target);
            prere = strtok(NULL, "\\");
            if (!prere)
                prere = " ";
            char *item_prere = malloc(sizeof(prere) * strlen(prere));
            item_prere = strcpy(item_prere, prere);

            recipe = calloc(1, sizeof(struct list));
            if (!recipe)
                errx(1, "exhausted ");

            long old_pos = ftell(g_opt->makefile);
            while ((read = getline(&line, &len, g_opt->makefile)) != -1)
            {
                if (line && line[0] == '\t')
                {
                    trim_n(line);
                    add_recipe(recipe, line);
                }
                else
                {
                    fseek(g_opt->makefile, old_pos, SEEK_SET);
                    break;
                }
            }
            add(item_target, item_prere, recipe);
        }
    }
    free(line);
}

int getmakefile()
{
    parse_var();
    parse_rule();
    return 0;
}
