#pragma once

#include "minimake.h"

struct option_sh
{
	bool all;

	bool f;
	char *file;

	bool h;

	FILE *makefile;

	struct data *table;
	struct rule *rule;
};

void print_usage(void);

struct option_sh *opt_parse(int argc, char *argv[]);
