#include "minimake.h"

struct option_sh *g_opt;

static void open_makefile(void)
{
    FILE *makefile;
    if (!g_opt->f)
    {
        makefile = fopen("GNUmakefile", "r");
        if (makefile == NULL)
        {
            makefile = fopen("makefile", "r");
            if (makefile == NULL)
            {
                makefile = fopen("Makefile", "r");
                if (makefile == NULL)
                {
                    free(g_opt);
                    errx(2,"*** No targets specified and no makefile found.Stop.");
                }
            }
        }
    }
    else
    {
        makefile = fopen(g_opt->file, "r");
        if (makefile == NULL)
        {
            free(g_opt);
            errx(2,"*** No targets specified and no makefile found. Stop.");
        }
    }
    g_opt->makefile = makefile;
}

static void at_exit(void)
{
    clear();
    relieve();
    fclose(g_opt->makefile);
    free(g_opt);
}

int main(int argc, char *argv[])
{
    int return_value = 0;
    g_opt = opt_parse(argc, argv);
    if (g_opt->h)
    {
        print_usage();
        return 0;
    }
    open_makefile();
    if (getmakefile(g_opt->makefile) == 2)
            return_value = 2;
    return_value = exec(argc, argv);
    at_exit();
    return return_value;
}
