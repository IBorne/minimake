#include "exec.h"

static char **to_array(char *line)
{
    char **array = malloc(sizeof(char * ) * 4);
    array[0] = "/bin/sh";
    array[1] = "-c";
    line = expand(line);
    array[2] = line;
    array[3] = NULL;
    return array;
}

static bool check_target(struct rule *rule)
{
    if (rule->target && !rule->prere)
        return (access(rule->target, F_OK) == -1);
    else
    {
        struct stat filestat;
        struct stat targetstat;
        stat(rule->target, &targetstat);
        time_t target_time = targetstat.st_mtime;
        char *token = strtok(rule->prere, " ");
        while (token)
        {
            stat(token, &filestat);
            time_t prere_time = filestat.st_mtime;
            if (difftime(prere_time, target_time) <= 0)
                return false;
            token = strtok(NULL, " ");
        }
    }
    return false;
}

static bool check_up_to_date(int i, char *argv[])
{
    for (int j = (g_opt->f) ? 3 : 1; j < i; j++)
    {
        if (strcmp(argv[i], argv[j]) == 0)
        {
            printf("./minimake: '%s' is up to date.\n", argv[i]);
            return true;
        }
    }
    return false;
}

int exec(int argc, char *argv[])
{
    int return_value = 0;
    int i = 1;
    if (g_opt->f)
        i = 3;
    for (; i < argc; i++)
    {
        if (check_up_to_date(i, argv))
            continue;
        struct rule *r = retrieve(argv[i]);
        if (!r)
        {
            warnx("*** No rule to make target '%s'. Stop.", argv[i]);
            return_value = 2;
            break;
        }
        struct list *recipe = r->recipe->next;
        if (!recipe)
        {
            printf("./minimake: Nothing to be done for '%s'.\n", \
            argv[i]);
            continue;
        }
        if (check_target(r))
        {
            printf("./minimake: '%s' is up to date.\n", argv[i]);
            break;
        }
        while (recipe)
        {
            char **arr = to_array(recipe->line);
            if (recipe && recipe->line)
                printf("%s\n", recipe->line);
            //fork
            pid_t pid = fork();
            if (pid == 0)
            {
                execvp(arr[0], arr);
            }
            else if (pid > 0)
            {
                int status = 0;
                wait(&status);
                if (status != 0)
                {
                    warnx("*** Recipe for target '%s' failed", argv[i]);
                    return_value = 2;
                    break;
                }
            }
            else
            {
                warnx("fork failed!");
                return 1;
            }
            //
            free(arr);
            recipe = recipe->next;
        }
    }
    return return_value;
}