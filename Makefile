CC=gcc
CFLAGS= -pedantic -Werror -Wall -Wextra -std=c99 -g -fsanitize=address
LDFLAGS= -lasan
SRC = src/opt_parser.c src/hashmap.c src/getline.c 
SRC+= src/rulemap.c src/main.c src/exec.c src/swap.c
OBJ = ${SRC:.c=.o}
BIN = minimake
.PHONY: all clean

all: ${BIN}
${BIN}: ${OBJ}
	${CC} ${CFLAGS} ${LDFLAGS} -o $@ $^

check:
	./test/test.sh

clean:
	${RM} ${OBJ} ${BIN}
