#!/bin/sh
make clean
make -q
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
echo #
echo -e "${GREEN}STARTING TEST SUITE${NC}"
echo #

TEST_SUITE ()(
    o1=$(make -q -f Makefile.defense -f ${ARG})
    o2=$(./minimake -f ../Makefile.defense -f ${ARG})
    if test "$o1" = "$o2" ;then
        echo -e "${RED}ouch${NC}     ./minimake ${ARG}"
    else
        echo -e ""${GREEN}alright${NC} ./minimake ${ARG}
    fi
)

ARG='echo'
TEST_SUITE

echo #
echo -e "${GREEN}END TEST SUITE${NC}"
echo #
